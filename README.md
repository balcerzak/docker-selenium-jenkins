# Docker: Selenium Grid & Jenkins #

### Core components ###

* Selenium Grid `http://{host}:4444/grid/console`
* Chrome node `vnc://{host}:5901`
* Firefox node `vnc://{host}:5902`
* Jenkins `http://{host}:8080`

---

### Documentation ###

* Docker Compose: https://docs.docker.com/compose
* Selenium Grid: https://github.com/SeleniumHQ/docker-selenium
* Jenkins: https://jenkins.io

#### Docker commands ####

* build container `docker build -t {name} .`
* run container `docker run {name}`
* list containers `docker ps, docker ps -a`
* delete all containers `docker rm $(docker ps -a -q)`
* delete all images `docker rmi $(docker images -q)`

#### Docker Compose commands ####

* build and run `docker-compose up -d`
* start or stop `docker-compose start|stop`
* scale `docker-compose scale {node_name}={number}`
